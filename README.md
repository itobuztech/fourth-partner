Push Plugin and File Opener Android Support Issue

1. Please edit 'plugins/cordova-plugin-file-opener2/plugin.xml'#34 from,
```
<framework src="com.android.support:support-v4:+" />
```

to,
```
<framework src="com.android.support:support-v4:26+" />
```

2. Remove and add platform
```
ionic cordova platform rm android
ionic cordova platform add android@latest
```
