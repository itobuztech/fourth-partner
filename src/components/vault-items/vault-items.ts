import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';


@Component({
  selector: 'vault-items',
  templateUrl: 'vault-items.html'
})

export class VaultItemsComponent implements OnChanges{
  @Input() vaultItems;
  @Output() open: EventEmitter<any> = new EventEmitter();
  viewList = false;

  constructor(
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  openFolder(item, index): void {
    this.open.emit({item, index});
  }

  
}
