import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'user-avatar',
  template: `{{initials}}`
})
export class UserAvatarComponent implements OnChanges{
  @Input() name: string;
  private initials: string;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.name && changes.name.currentValue)
    this.initials = this.name.charAt(0);
  }
}
