import { Directive, HostListener, Input } from '@angular/core';

import { Diagnostic } from '@ionic-native/diagnostic';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File, FileEntry } from '@ionic-native/file';
import { Platform, AlertController, LoadingController, ToastController } from 'ionic-angular';
import * as mime from 'mime-types';

@Directive({
  selector: '[download-open]' // Attribute selector
})
export class DownloadOpenDirective {
  @Input('download-open') url: string;
  @Input() isActive: string;
  @Input() subFolder: string;
  @Input() name: string;

  @HostListener('click') onClick () {
    if (this.platform.is('ios')) {
      this.checkFileExists();
    } else {
      this.diagnostic
        .getPermissionsAuthorizationStatus(this.diagnostic.permissionGroups.STORAGE)
        .then(response => {
          this.checkPermission(response);
        }, () => {});
    }
  }

  constructor (
    private alertController: AlertController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private platform: Platform,
    private diagnostic: Diagnostic,
    private file: File,
    private fileOpener: FileOpener,
    private transfer: FileTransfer
  ) { }

  private checkPermission (response) {
    if (
      (response[this.diagnostic.permission.READ_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.GRANTED &&
      response[this.diagnostic.permission.WRITE_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.GRANTED) ||
      (response[this.diagnostic.permission.READ_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE &&
      response[this.diagnostic.permission.WRITE_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE)
    ) {
      this.checkFileExists();
    } else if (
      response[this.diagnostic.permission.READ_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.NOT_REQUESTED &&
      response[this.diagnostic.permission.WRITE_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.NOT_REQUESTED
    ) {
      this.requestPermission();
    } else if (
      response[this.diagnostic.permission.READ_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.DENIED &&
      response[this.diagnostic.permission.WRITE_EXTERNAL_STORAGE] === this.diagnostic.permissionStatus.DENIED
    ) {
      this.alertController.create({
        title: 'Storage Request',
        subTitle: 'Please allow Fourth Partner to store files on your device.',
        buttons: [{
          text: 'Cancel'
        }, {
          text: 'OK',
          handler: this.requestPermission.bind(this)
        }]
      }).present();
    } else {
      this.goToAppSettings()
    }
  }

  private requestPermission () {
    this.diagnostic.requestRuntimePermissions(this.diagnostic.permissionGroups.STORAGE).then(response => {
      this.checkPermission(response);
    }, err => { });
  }

  private goToAppSettings () {
    this.alertController.create({
      title: 'Storage Request',
      subTitle: 'Please allow Fourth Partner to store files on your device.',
      buttons: [{
        text: 'Cancel'
      }, {
        text: 'OK',
        handler: () => {
          this.diagnostic.switchToSettings();
        }
      }]
    }).present();
  }

  private makePath(): string {
    let path: string;
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.externalRootDirectory;
    }
    path += 'FourthPartner/';

    if (this.subFolder) {
      path += `${this.subFolder}/`
    }

    const extension = this.name.substr(this.name.lastIndexOf('.'), this.name.length);
    const name = this.name.substr(0, this.name.lastIndexOf('.'));
    path += name.trim().replace(/[^\w\s-]/gi, '').replace(/ +/g,"_");
    path += extension;

    return path;
  }

  private checkFileExists () {
    const path = this.makePath();

    window['resolveLocalFileSystemURL'](path, (exists) => {
      this.openFile(exists);
    }, () => {
      this.downloadFile(path);
    });
  }

  private downloadFile (path) {
    const loading = this.loadingController.create();
    loading.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    let droidBackButton;
    if (this.platform.is('android')) {
      droidBackButton = this.platform.registerBackButtonAction(() => {
        fileTransfer.abort();
        loading.dismiss();
      }, 700);
    }

    fileTransfer.download(encodeURI(this.url), path).then((entry: FileEntry) => {
      loading.dismiss();
      if (this.platform.is('android')) {
        droidBackButton();
      }
      this.openFile(entry);
    }, (error) => {
      loading.dismiss();
      if (this.platform.is('android')) {
        droidBackButton();
      }
    });
  }

  private openFile (entry) {
    let file;
    const extension = this.url.substr(this.url.lastIndexOf('.'), this.url.length);
    if (this.platform.is('ios')) {
      file = entry.toURL();
    } else {
      file = entry.toInternalURL();
    }

    this.fileOpener.open(file, this.getMimeType(extension)).then(
      response => {},
      error => {
        this.toastController.create({
          message: 'This kind of file is not supported by your device.',
          dismissOnPageChange: false,
          duration: 3000,
          showCloseButton: true,
          closeButtonText: 'OK'
        });
      });
  }

  private getMimeType (extension: string): string {
    return mime.lookup(extension)
  }
}
