import { Component, Input } from '@angular/core';

@Component({
  selector: 'lists',
  templateUrl: 'lists.html'
})
export class ListsComponent {
  @Input() listItems: Array<any> = [];

  constructor() {

  }
  ngOnInit() {
    
  }

}
