import { NgModule } from '@angular/core';
import { ListsComponent } from './lists/lists';
import { IonicModule } from 'ionic-angular';
import { VaultItemsComponent } from './vault-items/vault-items';
import { VaultItemComponent } from './vault-item/vault-item';
import { ConversationItemComponent } from './conversation-item/conversation-item';
import { ProgressbarComponent } from './progressbar/progressbar';
import { ProjectexecutionComponent } from './projectexecution/projectexecution';
import { DownloadOpenDirective } from './download-open';
import { SubStagesDirective } from './sub-stages';
import { MomentModule } from 'angular2-moment';
import { UserAvatarComponent } from './user-avatar/user-avatar';
import { ProjectComponent } from './project/project';
import { DiscussionsFabComponent } from './discussions-fab/discussions-fab';
import { AlertsFabComponent } from './alerts-fab/alerts-fab';
@NgModule({
	declarations: [
    ListsComponent,
    VaultItemsComponent,
    VaultItemComponent,
    ConversationItemComponent,
    ProgressbarComponent,
    ProjectexecutionComponent,
    DownloadOpenDirective,
    SubStagesDirective,
    UserAvatarComponent,
    ProjectComponent,
    DiscussionsFabComponent,
    AlertsFabComponent,
  ],
	imports: [
    IonicModule,
    MomentModule
	],
	exports: [
    ListsComponent,
    VaultItemsComponent,
    VaultItemComponent,
    ConversationItemComponent,
    ProgressbarComponent,
    ProjectexecutionComponent,
    DownloadOpenDirective,
    UserAvatarComponent,
    ProjectComponent,
    DiscussionsFabComponent,
    AlertsFabComponent
  ]
})
export class ComponentsModule {}
