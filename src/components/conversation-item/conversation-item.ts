import { Component, Input } from '@angular/core';
import { conversation } from '../../interface/conversation';

@Component({
  selector: 'conversation-item',
  templateUrl: 'conversation-item.html'
})
export class ConversationItemComponent {
  @Input() serviceItem: conversation;

  constructor() {
  }

}
