import { Directive, AfterViewInit, Input, ElementRef, HostListener } from '@angular/core';
import { AlertController, AlertButton, NavController } from 'ionic-angular';

declare var d3pie;

@Directive({
  selector: '[sub-stages]'
})
export class SubStagesDirective implements AfterViewInit {
  @Input('sub-stages') data;
  @Input() parentVault = '';
  @HostListener('click', ['$event']) onClick (e: Event) {
    e.stopPropagation();
  }

  constructor (
    private elementRef: ElementRef,
    private alertCtrl: AlertController,
    private navController: NavController
  ) { }

  ngAfterViewInit () {
    const totalDone = this.data.reduce((a, d) => {
      if (d.status === 2) {
        return a + 1;
      }
      return a;
    }, 0);

    const title = Math.round((totalDone/this.data.length) * 100);

    new d3pie(this.elementRef.nativeElement, {
      header: {
        title: {
          text: 'PROJECT EXECUTION',
          fontSize: 15,
          font: 'Montserrat, sans-serif',
          color: '#324856'
        },
        subtitle: {
          text: `${title}%`,
          fontSize: 40,
          font: 'Montserrat, sans-serif',
          color: '#324856'
        },
        location: "pie-center"
      },
      size: {
        canvasWidth: this.elementRef.nativeElement.clientWidth,
        canvasHeight: this.elementRef.nativeElement.clientWidth,
        pieInnerRadius: '70%',
        pieOuterRadius: '95%'
      },
      data: {
        content: this.data.map((d, i) => {
          return {
            label: i + 1,
            value: (d.status === 1) ? 2 : 1,
            color: (d.status === 0) ? '#c1ccd9' : (d.status === 1) ? '#eb5717' : '#5ca560',
            name: d.name,
            description: d.description,
            vaults: d.vaults
          }
        })
      },
      labels: {
        outer: {
          format: 'none'
        },
        inner: {
          format: 'label',
        },
        mainLabel: {
          color: "#ffffff",
          font: 'Montserrat, sans-serif',
          fontSize: 15
        },
        gradient: {
          enabled: true,
          percentage: 75
        }
      },
      callbacks: {
        onClickSegment: (a) => {
          const buttons: AlertButton[] = [{
            role: 'cancel',
            text: 'OK'
          }];

          if (a.data.vaults && a.data.vaults.length) {
            buttons.push({
              text: 'Go to Vault',
              handler: () => {
                this.navController.push('vault', {
                  vault: a.data.vaults[0],
                  name: a.data.name
                });
              }
            });
          }

          this.alertCtrl.create({
            title: a.data.name,
            message: a.data.description || '<i>Description not provided</i>',
            buttons,
           cssClass: 'homeSingle'
          }).present();
        }
      }
    });
  }
}
