import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import { AuthserviceProvider } from './../../providers/authservice/authservice';
@Component({
  selector: 'alerts-fab',
  templateUrl: 'alerts-fab.html'
})
export class AlertsFabComponent implements OnInit, OnDestroy {
  alertNotificationsCount: any;
  notificationData: any;

  constructor(
    private navController: NavController,
    private authProvider: AuthserviceProvider
  ) { }

  ngOnInit() {
    this.notificationData = Observable.interval(1 * 20 * 1000)
      .startWith(3 * 60)
      .switchMap(() => this.authProvider.getAlertNotificationCount())
      .subscribe((res: any) => {
        this.alertNotificationsCount = res.unreadNotifications;
        console.log('this.alertNotificationsCount', this.alertNotificationsCount);
      }, err => {
        console.log(err);
      })
  }

  showAlerts() {
    this.navController.push('alert')
      .then(() => this.alertNotificationsCount = 0)
      .catch((err) => console.log(err))
  }

  ngOnDestroy() {
    this.notificationData.unsubscribe();
  }
}
