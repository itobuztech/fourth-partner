import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import { AuthserviceProvider } from './../../providers/authservice/authservice';

@Component({
  selector: 'discussions-fab',
  templateUrl: 'discussions-fab.html'
})
export class DiscussionsFabComponent implements OnInit, OnDestroy {
  discussionNotificationsCount: any;
  notificationData: any;

  constructor(
    private navController: NavController,
    private authProvider: AuthserviceProvider
  ) { }

  ngOnInit() {
    this.notificationData = Observable.interval(1 * 20 * 1000)
      .startWith(3 * 60)
      .switchMap(() => this.authProvider.getDiscussionNotificationCount())
      .subscribe((res: any) => {
        this.discussionNotificationsCount = res.notificationsCount;
        console.log('this.discussionNotificationsCount', this.discussionNotificationsCount);
      }, err => {
        console.log(err);
      })
  }

  showDiscussions() {
    this.navController.push('discussion')
      .then(() => this.discussionNotificationsCount = 0)
      .catch((err) => console.log(err))
  }

  ngOnDestroy() {
    this.notificationData.unsubscribe();
  }
}
