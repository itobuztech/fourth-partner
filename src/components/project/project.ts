import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import 'rxjs/add/operator/filter';


@Component({
  selector: 'project',
  templateUrl: 'project.html'
})
export class ProjectComponent implements OnInit{
  projects;
  selectedProject: any = {};

  constructor(
    private alertCtrl: AlertController,
    public menuCtrl: MenuController,
    private authProvider: AuthserviceProvider
  ) {}

  ngOnInit() {
    this.authProvider.profile$
      .filter(profile => profile)
      .subscribe(profile => {
        this.projects = profile.projects;
        if (this.projects.length > 1) {
          this.showSelectProjectAlert();
        }
      });
    this.setCurrentProject();
  }

  showSelectProjectAlert() {
    const selectedProject = JSON.parse(localStorage.getItem('project-alert'));
    if (!selectedProject) {
      this.selectProjectAlert();
      localStorage.setItem('project-alert', 'true');
    }
  }

  setCurrentProject () {
    this.authProvider.projectsSub
    .filter(project => project)
    .subscribe(project => {
      this.setProject(project);
    });
  }

  selectProjectAlert() {
    console.log('selectProjectAlert created');
    let alertInputs = [];
    this.projects.forEach(element => {
      alertInputs.push({type: 'radio', label: element.name, value: element});
    });
    alertInputs[0].checked = true;
    let alert = this.alertCtrl.create({
      title: 'Select Project',
      inputs: alertInputs,
      buttons: [
        {
          text: 'Ok',
          handler: data => {
            this.selectProject(data, false);
          }
        }
      ]
    });
    alert.present();
  }

  selectProject(project, callAnalytics) {
    this.setProject(project);
    if (callAnalytics) {
      const payload = {project_id:project.id, type:"project-switch"};
      this.authProvider.addAnalytics(payload);
    }
  }

  setProject(project) {
    console.log("Proj selected from component", project);
    this.selectedProject = project;
    this.authProvider.selectProject(this.selectedProject);
    this.menuCtrl.close();
  }

  compareFn(e1, e2): boolean {
    return e1 && e2 ? e1.id === e2.id : e1 === e2;
  }
}
