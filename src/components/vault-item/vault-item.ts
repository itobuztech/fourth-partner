import { Component, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { AuthserviceProvider } from '../../providers/authservice/authservice';

@Component({
  selector: 'vault-item',
  templateUrl: 'vault-item.html'
})
export class VaultItemComponent {
  @Input() item: any;
  @Input() isFile: boolean;
  @Input() analyticsInput: any;
  @Output() selectItem: EventEmitter<any> = new EventEmitter();
  @HostListener('click') onClick () {
    if (!this.isFile) {
      this.selectItem.emit();
    } else {
      this.addAnalytics(this.analyticsInput);
    }
  };

  images = {
    more: 'ion-ios-more',
    arrow: 'ion-ios-arrow-forward',
    pdf:  'assets/imgs/pdf.png',
    folder: 'assets/imgs/folder.png',
    doc: 'assets/imgs/doc.svg',
    docx: 'assets/imgs/doc.svg',
    jpg: 'assets/imgs/img.svg',
    jpeg: 'assets/imgs/img.svg',
    png: 'assets/imgs/img.svg',
    gif: 'assets/imgs/img.svg',
    xls: 'assets/imgs/xls-icon.svg',
    xlsx: 'assets/imgs/xls-icon.svg',
    zip: 'assets/imgs/xls-icon.svg',
    ppt: 'assets/imgs/ppt.svg',
    pptx: 'assets/imgs/ppt.svg'
  };

  constructor(
    private authProvider: AuthserviceProvider
  ) { }

  addAnalytics(payload) {
    payload.type = 'file-open';
    this.authProvider.addAnalytics(payload);
  }
}
