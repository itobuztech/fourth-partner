import { Component, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, LoadingController, Content } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/filter';

import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { DiscussionProvider }  from '../../providers/discussion/discussion';

@IonicPage({
  name: 'discussion'
})
@Component({
  selector: 'page-discussion',
  templateUrl: 'discussion.html',
})
export class DiscussionPage {
  private chatForm: FormGroup;
  messageData = [];
  id;
  discussions = [];
  isSub: boolean = true;
  subs: Subscription[] = [];
  project;
  @ViewChild('discussionChat') discussionChat: ElementRef;
  @ViewChild('content') content: Content;

  constructor(
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private authProvider: AuthserviceProvider,
    private discussionProvider: DiscussionProvider,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ionViewDidEnter() {
    this.chatForm = this.formBuilder.group({
      message: [''],
      project_id: ['']
    });

    this.getDiscussions();
  }

  ionViewWillLeave () {
    this.subs.map(sub => sub.unsubscribe());
  }

  getDiscussions () {
    this.subs.push(
      this.authProvider.projects$
      .filter(project => project !== null)
      .map(project => project.id)
      .do(project => {
        this.project = project;
        this.chatForm.get('project_id').setValue(project);
      })
      .switchMap(project => this.discussionProvider.getDiscussion(this.project))
      .subscribe(discussions => {
        this.discussions = discussions;
        this.content.resize();

        setTimeout(() => {
          const el = this.discussionChat.nativeElement.querySelector('.discussion__message:last-of-type')
          if (el) {
            el.scrollIntoView();
          }
        }, 300);
      }, err => {
        console.log('Message not loaded');
      })
    );
  }

  sendMessage() {
    let loader = this.loadingCtrl.create();
    loader.present();

    this.discussionProvider
      .sendDiscussion(this.chatForm.value)
      .finally(() => loader.dismiss())
      .subscribe(response => {
        const projectid = this.chatForm.get('project_id').value;
        this.chatForm.reset();
        this.chatForm.get('project_id').setValue(projectid);
        this.getDiscussions();
        this.changeDetectorRef.detectChanges();
      });
  }
}
