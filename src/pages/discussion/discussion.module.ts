import { NgModule } from '@angular/core';
import { MomentModule } from 'angular2-moment';

import { IonicPageModule } from 'ionic-angular';
import { DiscussionPage } from './discussion';


@NgModule({
  declarations: [
    DiscussionPage
  ],
  imports: [
    IonicPageModule.forChild(DiscussionPage),
    MomentModule
  ],
})
export class DiscussionPageModule {}
