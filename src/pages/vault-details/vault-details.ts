import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';


@IonicPage({
  name: 'VaultDetails'
})
@Component({
  selector: 'page-vault-details',
  templateUrl: 'vault-details.html',
})
export class VaultDetailsPage {

  constructor() {
  }

}
