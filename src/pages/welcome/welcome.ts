import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
@IonicPage({
  name: 'welcome'
})
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  filter = 'OpexModel';
  constructor() {}
}
