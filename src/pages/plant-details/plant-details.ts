import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { PlantsServiceProvider } from '../../providers/plants-service/plants-service';

@IonicPage({
  name: 'plantDetails'
})
@Component({
  selector: 'page-plant-details',
  templateUrl: 'plant-details.html',
})

export class PlantDetailsPage {
  showTab: string = 'Live';
  plants;
  subs = [];

  statsToday;
  energyToday;
  powerNow;
  irridation;
  inverters = [];
  meters;

  constructor (
    private authProvider: AuthserviceProvider,
    private plantsService: PlantsServiceProvider
  ) { }

  ionViewWillEnter () {
    this.subs.push (
      this.authProvider.projects$
        .filter(project => project !== null)
        .map(project => project.plant_id)
        .switchMap(plant => this.plantsService.getPlantDetail(plant))
        .subscribe(details => {
          this.plants = details;

          this.statsToday = {
            tl: details.slideDown.stat[0].value,
            kwh: details.slideDown.stat[1].value,
            pr: details.slideDown.stat[2].value
          };
          this.energyToday = {
            meter: details.slideDown.invertersData[0].inverters[0].energyToday,
            inverter: details.slideDown.invertersData[1].inverters[1].energyToday
          };
          this.powerNow = {
            current: details.slideDown.invertersData[1].inverters[1].powerNow,
            percent: details.slideDown.invertersData[1].inverters[1].powerNow / details.plantCapacity
          }
          this.irridation = details.slideDown.irridation;
          this.inverters = details.slideDown.invertersData[1].inverters;
          this.meters = details.slideDown.energyMeterNow[0].meters[0].value;
        }, e => {
          console.log(e);
        })
    );
  }

  ionViewWillLeave () {
    this.subs.map(sub => sub.unsubscribe());
  }
}
