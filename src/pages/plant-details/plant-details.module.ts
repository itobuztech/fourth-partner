import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MomentModule } from 'angular2-moment';

import { PlantDetailsPage } from './plant-details';
import { ArchiveComponent } from './components/archive/archive.component';
import { ChartComponent } from './components/chart/chart';
import { LineChartComponent } from './components/line-chart/line-chart';
import { ComponentsModule } from '../../components/components.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';


@NgModule({
  declarations: [
    PlantDetailsPage,
    ArchiveComponent,
    ChartComponent,
    LineChartComponent
  ],
  imports: [
    IonicPageModule.forChild(PlantDetailsPage),
    ComponentsModule,
    RoundProgressModule,
    MomentModule,
    ChartsModule
  ],
})
export class PlantDetailsPageModule {
  constructor () { }
}
