import { Component, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';

@Component({
  selector: 'chart',
  templateUrl: 'chart.html'
})
export class ChartComponent implements OnChanges {
  @Input() barGraphData: any;
  @ViewChild("baseChart")
  public chart: BaseChartDirective;

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    pan: {
      enabled: false,
      mode: 'xy',
      rangeMin: {
        x: null,
        y: null
      },
      rangeMax: {
        x: null,
        y: null
      }
    },
    zoom: {
        enabled: true,
        mode: 'xy',
    }
  };
  public barChartLabels:any[] = [];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = false;
 
  public barChartData:any[] = [];
  enrArray: any[] = [];
  invArray: any[] = [];
  allVal: any[] = [];
  finalArr: any[] = [];
  selectOptions = {
    title: 'Show Data'
  };

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const barGraphData = changes['barGraphData'];
    if (barGraphData) {
      this.barChartLabels = this.barGraphData.date;
      this.enrArray = [
        {data: this.barGraphData.meters, stack: 0, label: 'Energy Meters'},
        {data: this.barGraphData.pr, type: 'line', fill: false, pointRadius: 0, borderWidth: 2, stack: 2, label: 'PR'},
      ];
      this.barGraphData.inverterArray.forEach(element => {
        this.invArray.push({data: element.data, stack: 1, label: element.name});
      });
      this.finalArr = this.enrArray.concat(this.invArray);

      this.setPanRange(this.finalArr);
      this.barChartData = this.finalArr;
    }
  }

  onSelectChange(options) {
    let enrSelected = [];
    if (options.includes("Energy Meters")) {
      enrSelected.push(this.enrArray[0]);
    } 
    if (options.includes("PR")) {
      enrSelected.push(this.enrArray[1]);
    }
    if (options.includes("Inverters")) {
      this.invArray.forEach(el => {
        enrSelected.push(el);
      })
    }
    this.barChartLabels = this.barGraphData.date;
    this.barChartData = enrSelected;

    this.setPanRange(this.barChartData);
    this.reloadChart();
  }

  setPanRange(finalArr) {
    let allVal = [];
    finalArr.forEach((el) => {
      el.data.forEach(element => {
        allVal.push(element);
      });
    })
    this.barChartOptions.pan.rangeMin.y = Math.min(...allVal);
    this.barChartOptions.pan.rangeMax.y = Math.max(...allVal);
  }

  reloadChart() {
    if (this.chart !== undefined) {
      this.chart.ngOnDestroy();
      this.chart.chart = 0;
      
      this.chart.datasets = this.barChartData;
      this.chart.labels = this.barChartLabels;
      this.chart.ngOnInit();
    }
  }

}
