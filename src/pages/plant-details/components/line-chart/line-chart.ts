import { Component, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
@Component({
  selector: 'line-chart',
  templateUrl: 'line-chart.html'
})
export class LineChartComponent implements OnChanges {
  @Input() lineGraphData: any;
  @ViewChild("baseChart")
  public chart: BaseChartDirective;

  invArray: any[] = [];
  selectOptions = {
    title: 'Show Data'
  };
  public lineChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    pan: {
      enabled: false,
      mode: 'xy',
      rangeMin: {
        x: null,
        y: null
      },
      rangeMax: {
        x: null,
        y: null
      }
    },
    zoom: {
      enabled: true,
      mode: 'xy',
  }
  };
  public lineChartLabels:string[];
  public lineChartType:string = 'line';
  public lineChartLegend:boolean = false;
  public lineChartColors: any[] = [];
  
  public lineChartData:any[] = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const lineGraphData = changes['lineGraphData'];
    if (lineGraphData) {
      this.lineChartLabels = this.lineGraphData.date;
      this.invArray.push({data: this.lineGraphData.irradiation_kwh, fill: false, pointRadius: 0, borderWidth: 1, label: 'Irridation'});
      this.lineGraphData.inverters.forEach(element => {
        this.invArray.push({data: element.value, fill: false, pointRadius: 0, borderWidth: 1, label: element.key})
      });
      this.setPanRange(this.invArray);
      this.lineChartData = this.invArray;
    }
  }

  onSelectChange(options) {
    this.lineChartLabels = this.lineGraphData.date;
    this.lineChartData = options;
    this.setPanRange(this.lineChartData);
    this.reloadChart();
  }

  setPanRange(finalArr) {
    let allVal = [];
    finalArr.forEach((el) => {
      el.data.forEach(element => {
        allVal.push(element);
      });
    })
    this.lineChartOptions.pan.rangeMin.y = Math.min(...allVal);
    this.lineChartOptions.pan.rangeMax.y = Math.max(...allVal);
  }

  reloadChart() {
    if (this.chart !== undefined) {
       this.chart.ngOnDestroy();
       this.chart.chart = 0;

       this.chart.datasets = this.lineChartData;
       this.chart.labels = this.lineChartLabels;
       this.chart.ngOnInit();
    }
  }
}
