import { Component, OnInit } from '@angular/core';

import { PlantsServiceProvider } from '../../../../providers/plants-service/plants-service';
import { } from '../../../../providers/'


@Component({
  selector: 'plant-details-archive',
  templateUrl: 'archive.component.html'
})

export class ArchiveComponent implements OnInit {
  filter = 'month';
  barChart;
  lineChart;
  now = new Date();
  public event = {
    dateStarts: `${this.now.getMonth() + 1}-${this.now.getDate()}-${this.now.getFullYear()}`
  }
  barDate = this.now;
  lineDate = this.now;
  barDateLabel = this.now.toLocaleDateString("en-US", { year: 'numeric', month: 'long' });
  lineDateLabel = this.now.toLocaleDateString("en-US", { year: 'numeric', month: 'long', day: 'numeric' });
  plantId: string = '593119a3ba03e26520bd63ab';


  constructor(
    private plantsService: PlantsServiceProvider
  ) { }

  ngOnInit() {
    // this.plantId = this.plantsService.getplantId();
    this.getPlantsGraph('month', this.event.dateStarts);
    this.getPlantLineChart('inverter', this.event.dateStarts);
  }

  prevBarDate() {
    if (this.filter == 'month') {
      this.barDate.setMonth(this.barDate.getMonth()-1);
    } else {
      this.barDate.setFullYear(this.barDate.getFullYear()-1);
    }
    this.changeGraph(this.filter, this.barDate);
  }

  nxtBarDate() {
    if (this.filter == 'month') {
      this.barDate.setMonth(this.barDate.getMonth()+1);
    } else if (this.filter == 'year') {
      this.barDate.setFullYear(this.barDate.getFullYear()+1);
    }
    this.changeGraph(this.filter, this.barDate);
  }

  /**
   * Change Graph tab
   * @param type: string
   */
  public changeGraph(type: string, date?) {
    this.filter = type;
    this.barDate = date ? new Date(date) : this.now;
    this.event.dateStarts = `${this.barDate.getMonth() + 1}-${this.barDate.getDate()}-${this.barDate.getFullYear()}`;
    this.barDateLabel = (type == 'month') ? this.barDate.toLocaleDateString("en-US", { year: 'numeric', month: 'long' }) : this.barDate.toLocaleDateString("en-US", { year: 'numeric'});

    console.log("this.event.dateStarts", this.event.dateStarts);
    this.getPlantsGraph(type, this.event.dateStarts);
  }

  /**
   * Get plants chart
   * @param type: string
   * @param date: string
   */
  
  private getPlantsGraph(type: string, date: string) {
    this.barChart = null;
    this.plantsService.getPlantChart(this.plantId, type, date)
      .subscribe(res => {
        this.barChart = res;
        console.log("this.barChart", this.barChart);
      })
  }

  prevLineDate() {
    this.lineDate.setDate(this.lineDate.getDate()-1);
    this.onUpdateLineDate(this.lineDate);
  }

  nxtLineDate() {
    this.lineDate.setDate(this.lineDate.getDate()+1);
    this.onUpdateLineDate(this.lineDate);
  }

  onUpdateLineDate(date) {
    console.log("date", date);
    this.lineDate = new Date(date);
    this.event.dateStarts = `${this.lineDate.getMonth() + 1}-${this.lineDate.getDate()}-${this.lineDate.getFullYear()}`;
    this.lineDateLabel = this.lineDate.toLocaleDateString("en-US", { year: 'numeric', month: 'long', day: 'numeric' });
    console.log("this.lineDateLabel", this.lineDateLabel);
    this.getPlantLineChart('inverter', this.event.dateStarts);
  }

  private getPlantLineChart(type: string, date: string) {
    this.lineChart = null;
    this.plantsService.getPlantChart(this.plantId, type, date)
      .subscribe(res => {
        this.lineChart = res;
        console.log("this.lineChart", this.lineChart);
      })
  }
}