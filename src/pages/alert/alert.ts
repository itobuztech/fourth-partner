import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice'
import 'rxjs/add/operator/finally';

@IonicPage({
  name: 'alert'
})
@Component({
  selector: 'page-alert',
  templateUrl: 'alert.html',
})
export class AlertPage {
  title: string = 'Alerts';
  alertData: any = [];
  project;
  
  constructor(
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private authserviceProvider: AuthserviceProvider
  ) {
  }

  ionViewDidEnter () {
    this.authserviceProvider.projects$
    .filter(project => project !== null)
    .subscribe(project => {
      this.project = project;
    });
    this.getAlerts(this.project.id);
  }

  getAlerts(projectId) {
    let loader = this.loadingCtrl.create();
    loader.present();
    this.authserviceProvider.getAlerts(projectId)
      .finally(() => loader.dismiss())
      .subscribe(res => {
        this.alertData = res;
      },
      err => {
      });
  }

  goToPage (alertType, vaultId) {
    if (alertType === 'vaults') {
      this.navCtrl.push('vault', {
        vault: vaultId
      });
    } else {
      this.navCtrl.setRoot('home');
    }

  }
}
