import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, LoadingController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/finally';

import { AuthserviceProvider } from '../../providers/authservice/authservice'

@IonicPage({
  name: 'contact'
})
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage implements OnInit {
  contactForm: FormGroup;
  private pattern = {
    email: /[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}/
  };

  constructor(
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private toastController: ToastController,
    private authserviceProvider: AuthserviceProvider
  ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.pattern.email)])],
      company: [''],
      designation: [''],
      reason: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  send(value, valid: boolean) {
    if (valid) {
      let loader = this.loadingCtrl.create();
      loader.present();
      this.authserviceProvider.postEnquiry(value)
        .finally(() => loader.dismiss())
        .subscribe(res => {
          this.contactForm.reset();
          this.toastController.create({
            message: res.data,
            dismissOnPageChange: true,
            duration: 3000
          }).present();
        },
        err => {
          for (let errs in err.errors) {
            this.contactForm.get(errs).setErrors({
              server: err.errors[errs][0]
            });
          }
        });
    } else {
      for (const control in this.contactForm.controls) {
        if (this.contactForm.controls.hasOwnProperty(control)) {
          this.contactForm.controls[control].markAsTouched();
        }
      }
    }
  }
}
