import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutSolarPage } from './about-solar';

@NgModule({
  declarations: [
    AboutSolarPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutSolarPage),
  ],
})
export class AboutSolarPageModule {}
