import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage({
  name: 'aboutSolar'
})
@Component({
  selector: 'page-about-solar',
  templateUrl: 'about-solar.html',
})
export class AboutSolarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
