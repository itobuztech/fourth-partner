import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, LoadingController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/finally';

import { AuthserviceProvider } from '../../providers/authservice/authservice';

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  private pattern = {
    email: /[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}/
  };

  constructor(
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private toastController: ToastController,
    private navController: NavController,
    private authService: AuthserviceProvider
  ) { }

  // ionViewCanEnter () {
  //   if (localStorage.getItem('fourthpartner_token')) {
  //     return false;
  //   }
  //   return true;
  // }

  ngOnInit () {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.pattern.email)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    });
  }

  onLogin(value, valid: boolean) {
    if(valid) {
      let loader = this.loadingCtrl.create();
      loader.present();

      this.authService.login(value)
        .finally(() => loader.dismiss())
        .subscribe(
          () => {},
          err => {
            this.toastController.create({
              message: err.errors || 'Unable to connect to server',
              dismissOnPageChange: true,
              duration: 3000
            }).present();
          }
        );
    } else {
      this.loginForm.get('email').markAsTouched();
      this.loginForm.get('password').markAsTouched();
    }
  }

  goBack () {
    this.navController.setRoot('welcome');
  }
}
