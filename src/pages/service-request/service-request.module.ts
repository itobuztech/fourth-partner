import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MomentModule } from 'angular2-moment';

import { ServiceRequestPage } from './service-request';

@NgModule({
  declarations: [
    ServiceRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceRequestPage),
    MomentModule
  ],
})
export class ServiceRequestPageModule {}
