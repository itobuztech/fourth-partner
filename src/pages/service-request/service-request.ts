import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { DataProvider } from '../../providers/dataService';
@IonicPage({
  name: 'service-request'
})
@Component({
  selector: 'page-service-request',
  templateUrl: 'service-request.html',
})
export class ServiceRequestPage {
  serviceData = [];
  
  orangeFolder = 'assets/imgs/orange-folder.png';
  blueFolder = 'assets/imgs/blue-folder.png';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private dataProvider: DataProvider,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.getRequest();
  }

  gotoConversation(id) {
    this.navCtrl.push('ServiceRequestConversation', {id});
  }

  createServiceRequest() {
    let createModal = this.modalController.create('createRequest');
    createModal.present();
    createModal.onDidDismiss(this.getRequest.bind(this));
  }

  getRequest() {
    this.dataProvider.getServiceRequest()
      .subscribe(res=> {
        let data = res.data;
        this.serviceData = data.reverse();
      })
  }
}
