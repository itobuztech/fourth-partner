import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';

import { VaultProvider } from '../../providers/vaultsService';
import { AuthserviceProvider } from '../../providers/authservice/authservice';


@IonicPage({
  name: 'vault'
})
@Component({
  selector: 'page-vault',
  templateUrl: 'vault.html',
})
export class VaultPage {
  vaultData;
  showVaultData;
  subs: Subscription[] = [];
  vaultMap: number[] = [];
  droidBackButton: Function;
  projectStatus;

  constructor (
    private platform: Platform,
    private navParams: NavParams,
    private navController: NavController,
    private vaultProvider: VaultProvider,
    private authProvider: AuthserviceProvider,
    private loadingCtrl: LoadingController
  ) { }

  ionViewWillEnter () {
    const vaultID = this.navParams.get('vault');
    this.getVaultList(vaultID);

    this.droidBackButton = this.platform.registerBackButtonAction(() => {
      if (this.vaultMap.length) {
        this.goBack();
      } else {
        if (this.projectStatus === 'open') {
          this.navController.setRoot('home');
        } else {
          this.navController.setRoot('service-request');
        }
      }
    }, 600);
  }

  ionViewWillLeave () {
    this.subs.map(sub => sub.unsubscribe());
    this.droidBackButton();
  }

  getVaultList(vault?) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });

    this.subs.push (
      this.authProvider.projects$
        .filter(project => project !== null)
        .do(project => this.projectStatus = project)
        .switchMapTo(Observable.fromPromise(loading.present()))
        .switchMap(project => this.vaultProvider.getVaults(this.projectStatus.id))
        .subscribe((res: any) => {
          this.vaultData = res;
          if (vault && res) {
            const finder = this.findDeep(this.vaultData.children, vault);

            if (finder.foundItem) {
              this.showVaultData = finder.foundItem;
              this.vaultMap = finder.foundIndex;
            } else {
              this.show();
            }
          } else {
            this.show();
          }

          loading.dismiss();
        }, err => {
          loading.dismiss();
        })
    );
  }

  private findDeep (arr: any[], id) {
    const findIndex = (children) => {
      let i = -1;
      const found = children.filter((val, index) => {
        if (val.id === +id) {
          i = index;
          return true;
        }
        return false;
      })[0];

      return { i, found };
    }

    let foundIndex = [];
    let foundItem;
    arr.forEach((a, index) => {
      if (a.id === +id) {
        foundIndex.push(index);
        foundItem = a;
      } else if (a.children && a.children.length) {
        const finder = findIndex(a.children)
        if (finder.i > -1) {
          foundIndex.push(index, finder.i);
          foundItem = finder.found;
        }
      }
    });

    return {foundIndex,foundItem}
  }

  show (event?) {
    if (event) {
      this.showVaultData = event.item;
      this.vaultMap.push(event.index);
    } else {
      let vaultin = this.vaultData;
      if (this.vaultMap.length) {
        this.vaultMap.forEach(map => {
          vaultin = vaultin.children[map];
        })
      }
      this.showVaultData = vaultin;
    }
  }

  goBack () {
    this.vaultMap.splice(this.vaultMap.length - 1, 1);
    if (this.vaultMap.length) {
      let item = this.vaultData;
      for (let index = 0; index < this.vaultMap.length; index++) {
        item = item.children[this.vaultMap[index]];
      }
      this.showVaultData = item;
    } else {
      this.showVaultData = this.vaultData;
    }
  }

  getAll () {
    this.getVaultList();
    this.vaultMap = [];
  }
}
