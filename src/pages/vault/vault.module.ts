import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VaultPage } from './vault';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VaultPage,
  ],
  imports: [
    IonicPageModule.forChild(VaultPage),
    ComponentsModule,
  ],
})
export class VaultPageModule {}
