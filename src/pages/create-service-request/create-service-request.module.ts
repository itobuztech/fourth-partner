import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateServiceRequestPage } from './create-service-request';

@NgModule({
  declarations: [
    CreateServiceRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateServiceRequestPage),
  ],
})
export class CreateServiceRequestPageModule {}
