import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { DataProvider } from '../../providers/dataService';


@IonicPage({
  name: 'createRequest'
})
@Component({
  selector: 'page-create-service-request',
  templateUrl: 'create-service-request.html',
})
export class CreateServiceRequestPage implements OnInit {
  requestForm: FormGroup;
  data: any;
  requestId;
  serviceType;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private dataProvider: DataProvider,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.requestForm = this.formBuilder.group({
      title: ['', Validators.required],
      service_request_type_id: ['', Validators.required]
    })

    this.getType();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  createRequest() {
    const prevSelectedProject = JSON.parse(localStorage.getItem('project'));
    this.requestForm.value.project_id = prevSelectedProject.id;
    this.dataProvider.createServiceRequest(this.requestForm.value)
      .subscribe(res => {
        this.requestForm.reset();
        // this.viewCtrl.dismiss();
        this.navCtrl.pop();
      });
  }

  getType() {
    this.dataProvider.getServiceType()
      .subscribe(type => {
        this.serviceType = type;
      })
  }
}
