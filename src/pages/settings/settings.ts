import { Component, OnInit } from '@angular/core';
import { IonicPage, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import 'rxjs/add/operator/finally';

import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { Subscription } from 'rxjs/Subscription';

@IonicPage({
  name: 'Settings'
})
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  tab = 'profile';
  private passwordForm: FormGroup;
  private subs: Subscription[] = [];
  user;

  constructor (
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private authService: AuthserviceProvider,
  ) {}

  ngOnInit() {
    this.passwordForm = this.formBuilder.group({
      old_password: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      new_password: ['', Validators.minLength(5)],
      new_password_confirmation: ['', Validators.minLength(5)],
    }, {
      validator: this.validatePass,
      updateOn: 'blur'
    });
  }
  
  ionViewWillEnter () {
    this.subs.push (
      this.authService.profile$.filter(profile => profile !== null).subscribe(user => this.user = user)
    );
  }

  ionViewWillLeave () {
    this.subs.map(sub => sub.unsubscribe());
  }

  changePass(value: any, valid: boolean) {
    if (valid) {
      let loader = this.loadingCtrl.create();
      loader.present();
      this.authService.passwordReset(value)
        .finally(() => loader.dismiss())
        .subscribe(res => {
          this.passwordForm.reset();
          this.toastCtrl.create({
            message: res.data,
            duration: 3000,
            position: 'bottom'
          }).present();
        }, err => {
          this.toastCtrl.create({
            message: err.errors,
            duration: 3000,
            position: 'bottom'
          }).present();
        });
    } else {
      this.passwordForm.get('old_password').markAsTouched();
      this.passwordForm.get('new_password').markAsTouched();
      this.passwordForm.get('new_password_confirmation').markAsTouched();
    }
  }

  validatePass(group: FormGroup) {
    return group.get('new_password').value === group.get('new_password_confirmation').value ? null : {'mismatch': true};
  }

  logOut() {
    this.authService.logOut();
  }
}
