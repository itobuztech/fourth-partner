import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InitialProposalPage } from './initial-proposal';

@NgModule({
  declarations: [
    InitialProposalPage,
  ],
  imports: [
    IonicPageModule.forChild(InitialProposalPage),
  ],
})
export class InitialProposalPageModule {}
