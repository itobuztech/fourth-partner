import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage({
  name: 'initialProposal'
})
@Component({
  selector: 'page-initial-proposal',
  templateUrl: 'initial-proposal.html',
})
export class InitialProposalPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
