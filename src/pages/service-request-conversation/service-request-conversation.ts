import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/finally';

import { DataProvider } from '../../providers/dataService'

@IonicPage({
  name: 'ServiceRequestConversation',
  segment: 'ServiceRequestConversation/:id'
})
@Component({
  selector: 'page-service-request-conversation',
  templateUrl: 'service-request-conversation.html',
})
export class ServiceRequestConversationPage implements OnInit {
  serviceData;
  conversationGroup: FormGroup;
  getConversation;

  constructor (
    public navParams: NavParams,
    private loadingController: LoadingController,
    private dataProvider: DataProvider,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit () {
    const id = this.navParams.get('id');
    this.conversationGroup = this.formBuilder.group({
      message: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      service_request_id: [id]
    });
  }

  ionViewWillEnter() {
    const id = this.navParams.get('id');
    this.getRequestByID(id);
    this.getMessage(id);
    this.conversationGroup.get('service_request_id').patchValue(id);
  }

  getRequestByID(id) {
    const loader = this.loadingController.create();
    loader.present();

    this.dataProvider.getServiceRequestById(id)
      .finally(() => loader.dismiss())
      .subscribe(res => {
        this.serviceData = res;
        console.log(res)
      }, err => {})
  }

  doConversation(value, valid) {
    if(valid) {
      this.dataProvider.serviceConversation(value)
        .subscribe(res => {
          this.getMessage(value.service_request_id);
          this.conversationGroup.get('message').reset();
        },err => {
          console.log('err',err);
        })
    } else {
      this.conversationGroup.get('message').markAsTouched();
    }
  }

  getMessage(id) {
    this.dataProvider.getServiceMessage(id)
      .subscribe(res => {
        this.getConversation = res;
      }, err => {})
  }
}
