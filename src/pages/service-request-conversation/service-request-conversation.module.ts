import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceRequestConversationPage } from './service-request-conversation';
import { ComponentsModule } from '../../components/components.module';
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    ServiceRequestConversationPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceRequestConversationPage),
    ComponentsModule,
    MomentModule
  ],
})
export class ServiceRequestConversationPageModule {}
