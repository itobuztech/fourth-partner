import { Component, OnInit } from '@angular/core';
import { IonicPage, LoadingController, AlertController, AlertButton, NavController, Refresher } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/do';

import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { DataProvider } from '../../providers/dataService';

@IonicPage({
  name: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {
  private subs: Subscription[] = [];
  stages;
  completion: number = 0;
  project;

  constructor (
    private navController: NavController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private authserviceProvider: AuthserviceProvider,
    private dataProvider: DataProvider
  ) { }

  ngOnInit() {
    console.log('home page init');
    this.getStages();
  }

  ionViewCanEnter () {
    if (localStorage.getItem('fourthpartner_token')) {
      return true;
    }
    return false;
  }

  ionViewDidEnter() {
    this.refreshStages(null);
  }

  ionViewWillLeave () {
    this.subs.map(sub => sub.unsubscribe());
  }

  presentProposalModal (value) {
    const stage = this.stages[value];

    const buttons: AlertButton[] = [{
      role: 'cancel',
      text: 'OK'
    }];

    if (stage.vaults && stage.vaults.length) {
      buttons.push({
        text: 'Go to Vault',
        handler: () => {
          this.navController.push('vault', {
            vault: stage.vaults[0],
            name: stage.name
          });
        }
      });
    }

    let alert = this.alertCtrl.create({
      title: stage.name,
      message: stage.description || '<i>Description not provided</i>',
      buttons,
      cssClass: 'homeSingle'
    });

    alert.present();
  }

  getStages () {
    const loader = this.loadingCtrl.create({
      dismissOnPageChange: true
    });

    loader.present().then(() => {
      console.log('shwing home loader');
      this.subs.push (
        this.authserviceProvider
          .projects$
          .filter(project => project !== null)
          .do(project => this.project = project)
          .switchMap(() => this.dataProvider.getStages(this.project.id))
          .subscribe(stages => {
            loader.dismiss();
            this.storeStages(stages);
          }, err => {
            loader.dismiss();
          })
      );
    })
  }

  refreshStages (refresher: Refresher) {
    const prevSelectedProject = JSON.parse(localStorage.getItem('project'));
    this.dataProvider.getStages(prevSelectedProject.id)
      .subscribe(stages => {
        this.storeStages(stages);
        refresher && refresher.complete();
      })
  }

  private storeStages (stages) {
    this.stages = stages;

    let completedStagesLength: number = this.stages.filter(stages => stages.status === 2).length;
    let inProcessChildrens = this.stages.find(stages => stages.status === 1);
    inProcessChildrens = (inProcessChildrens && inProcessChildrens.children) ? inProcessChildrens.children : [];
    let inProcessChildrensLength = inProcessChildrens.filter(stages => stages.status === 2).length;

    const completed = ((completedStagesLength / this.stages.length) * 100) + ((inProcessChildrensLength * (inProcessChildrens.length / 100)) / 100);
    this.completion = Math.ceil(completed);
  }
}
