import { Component, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import {} from 'googlemaps';
import 'rxjs/add/operator/debounceTime';

import { INSTALLATIONS } from './installations';
import { MAPSTYLE } from './map-style';

@Component({
  selector: 'portfolio-installation',
  templateUrl: './installation.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortfolioInstallation {
  @ViewChild('viewMap') viewMap: ElementRef;
  private installations = INSTALLATIONS;
  markerListeners = [];
  selectedInstallation;
  range;
  rangeMin: any;
  rangeMax: any;
  markers: any[] = [];

  constructor (
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit () {
    const capacityRange = this.installations.map(installation => installation.plantCapacity);
    this.range = {
      max: Math.max(...capacityRange),
      min: Math.min(...capacityRange),
      selected: new FormControl()
    };

    this.range.selected.setValue({
      lower: ((this.range.max * 40) / 100),
      upper: ((this.range.max * 60) / 100)
    });

    this.initMap();

    this.range.selected
      .valueChanges
      .debounceTime(600)
      .subscribe(value => {
        console.log("range value", value);
        this.changeDetectorRef.detectChanges();
        this.filterMarkers();
      });
  }

  ngOnDestroy () {
    this.markerListeners.forEach(listener => {
      google.maps.event.removeListener(listener);
    });
  }

  private initMap () {
    let mapOptions: any = {
      center: new google.maps.LatLng(20.5937, 78.9629),
      zoom: 4,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      gestureHandling: 'cooperative',
      fullscreenControl: false,
      zoomControl: true,
      styles: MAPSTYLE
    };

    const gMap = new google.maps.Map(this.viewMap.nativeElement, mapOptions);

    this.installations.forEach(installation => {
      this.setMarker (gMap, installation);
    });

    this.filterMarkers();

    // This is required to trigger Google Maps internal recalculations.
    // Removing this will show greyed Maps area, on multiple loads
    google.maps.event.trigger(gMap, 'resize');
    // gMap.setCenter(latLng);
  }


  private setMarker(map, installation) {
    const latLng: google.maps.LatLng = new google.maps.LatLng(installation.location[0], installation.location[1]);

    const marker = new google.maps.Marker({
      map: map,
      position: latLng,
      optimized: true,
      visible: false,
      icon: 'assets/imgs/pin.svg'
    });

    marker['capacity'] = installation.plantCapacity;

    this.markers.push(marker);

    this.markerListeners.push (
      marker.addListener('click', () => {
        this.selectedInstallation = installation;
        this.changeDetectorRef.detectChanges();
      })
    );
  }

  private filterMarkers () {
    const value = this.range.selected.value;

    this.markers.forEach(marker => {
      marker.setVisible(marker.capacity >= value.lower && marker.capacity <= value.upper);
    });
  }

  close () {
    this.selectedInstallation = null;
    this.changeDetectorRef.detectChanges();
  }
}
