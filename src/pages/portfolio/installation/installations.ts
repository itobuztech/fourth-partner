export const INSTALLATIONS = [
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Mumbai',
    commissioningDate: '2015-03-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.112932,
      73.015147
    ],
    plantName: 'Glenmark MB',
    state: 'Maharashtra',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Banglore',
    commissioningDate: '2016-04-12T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.986145,
      77.478381
    ],
    plantName: 'Rinac B\'lore',
    state: 'Karnataka',
    plantCapacity: 150
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2016-06-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.519754,
      73.836454
    ],
    plantName: 'Gokhale Pune',
    state: 'Maharashtra',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Banglore',
    commissioningDate: '2016-04-03T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.974493,
      77.706608
    ],
    plantName: 'Bigbasket B\'lore',
    state: 'Karnataka',
    plantCapacity: 200
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Mumbai',
    commissioningDate: '2016-07-28T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.114292,
      72.895995
    ],
    plantName: 'ICICI Chandivili',
    state: 'Maharashtra',
    plantCapacity: 67
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2016-05-25T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.488345,
      73.996632
    ],
    plantName: 'DSK Pune',
    state: 'Maharashtra',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Banglore',
    commissioningDate: '2016-09-02T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.851031,
      77.676069
    ],
    plantName: 'Schneider B\'lore',
    state: 'Karnataka',
    plantCapacity: 120
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Badlapur',
    commissioningDate: '2016-09-23T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.173667,
      73.226186
    ],
    plantName: 'D - Mart (Badlapur)',
    state: 'Maharashtra',
    plantCapacity: 100.8
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raichur',
    commissioningDate: '2016-09-14T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      16.203917,
      77.348915
    ],
    plantName: 'Navodaya',
    state: 'Karnataka',
    plantCapacity: 500.3
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2016-09-27T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.513645,
      73.647797
    ],
    plantName: 'Pickwik Pune',
    state: 'Maharashtra',
    plantCapacity: 100.8
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Anklewshwar',
    commissioningDate: '2016-11-19T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      21.624765,
      73.013877
    ],
    plantName: 'Lupin GJ',
    state: 'Gujarat',
    plantCapacity: 30
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2016-10-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.149609,
      78.212049
    ],
    plantName: 'Symbiosis (Hyd)',
    state: 'Telengana',
    plantCapacity: 467.765
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Nagpur',
    commissioningDate: '2016-11-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.960227,
      79.014401
    ],
    plantName: 'TGPCOE',
    state: 'Maharashtra',
    plantCapacity: 120
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2016-11-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.642209,
      74.019189
    ],
    plantName: 'Pricol Pune',
    state: 'Maharashtra',
    plantCapacity: 369.6
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Jamnagar',
    commissioningDate: '2016-11-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.47602,
      70.055868
    ],
    plantName: 'Satya Sai School',
    state: 'Gujarat',
    plantCapacity: 33
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Talegaon',
    commissioningDate: '2016-12-25T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.735059,
      73.674516
    ],
    plantName: 'MIT Talegaon',
    state: 'Maharashtra',
    plantCapacity: 192.2
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Mulund',
    commissioningDate: '2017-01-01T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.163698,
      72.940402
    ],
    plantName: 'D - Mart (Mulund)',
    state: 'Maharashtra',
    plantCapacity: 320
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Mesra',
    commissioningDate: '2016-12-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      23.412101,
      85.439445
    ],
    plantName: 'BIT Ranchi',
    state: 'Jharkhand',
    plantCapacity: 300
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2017-01-17T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.575598,
      73.908371
    ],
    plantName: 'Symbiosis (Pune)',
    state: 'Maharashtra',
    plantCapacity: 207.9
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Murbad',
    commissioningDate: '2016-01-12T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.274009,
      73.396033
    ],
    plantName: 'Rinac(Murbad)',
    state: 'Maharashtra',
    plantCapacity: 200
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Gurgaon',
    commissioningDate: '2016-03-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.498725,
      77.022345
    ],
    plantName: 'Bestech',
    state: 'Haryana',
    plantCapacity: 268
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Manesar',
    commissioningDate: '2015-07-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.366823,
      76.910494
    ],
    plantName: 'Pricol (Manesar)',
    state: 'Haryana',
    plantCapacity: 151.3
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Perundurai',
    commissioningDate: '2017-03-01T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      11.235402,
      77.564597
    ],
    plantName: 'Britannia',
    state: 'Tamil Nadu',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Jaipur',
    commissioningDate: '2017-02-28T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.936102,
      75.92349
    ],
    plantName: 'LNMIT JP',
    state: 'Rajasthan',
    plantCapacity: 205
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2015-11-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.543851,
      73.904412
    ],
    plantName: 'Sky Lounge Lunkad',
    state: 'Maharashtra',
    plantCapacity: 12
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Baramati',
    commissioningDate: '2017-03-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      18.218249,
      74.60102
    ],
    plantName: 'Ferrero',
    state: 'Maharashtra',
    plantCapacity: 1500.9
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Dabhasa',
    commissioningDate: '2017-03-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.255728,
      73.038184
    ],
    plantName: 'Lupin Dabhasa',
    state: 'Gujarat',
    plantCapacity: 70
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Kurnool',
    commissioningDate: '2017-03-24T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      15.774142,
      78.057037
    ],
    plantName: 'GPREC',
    state: 'Andhra Pradesh',
    plantCapacity: 200
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Sanjan',
    commissioningDate: '2017-03-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.221081,
      72.826958
    ],
    plantName: 'Western (Sanjan)',
    state: 'Gujarat',
    plantCapacity: 448.87
  },
  {
    addressLineOne: 'Invertis University',
    addressLineTwo: 'Delhi Lucknow Highway',
    city: 'Bareilly',
    commissioningDate: '2017-03-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.292653,
      79.492833
    ],
    plantName: 'Invertis',
    state: 'Uttar Pradesh',
    plantCapacity: 800
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Taloja',
    commissioningDate: '2017-03-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      19.065204,
      73.118251
    ],
    plantName: 'Western (Taloja)',
    state: 'Maharshtra',
    plantCapacity: 500.85
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Perundarai',
    commissioningDate: '2017-03-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      11.270407,
      77.582621
    ],
    plantName: 'Walplast (Miraj - Perundarai)',
    state: 'Tamil Nadu',
    plantCapacity: 257
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Alandi',
    commissioningDate: '2017-05-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.675013,
      73.892053
    ],
    plantName: 'MIT Alandi 1',
    state: 'Maharashtra',
    plantCapacity: 209
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2017-05-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.424328,
      78.548435
    ],
    plantName: 'Fourth Partner Energy',
    state: 'Telengana',
    plantCapacity: 14.8
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Lucknow',
    commissioningDate: '2017-03-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.93201,
      80.980367
    ],
    plantName: 'Guru Gobind Singh',
    state: 'Uttar Pradesh',
    plantCapacity: 250
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Devrukh',
    commissioningDate: '2017-05-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.090868,
      73.589823
    ],
    plantName: 'Adler',
    state: 'Maharashtra',
    plantCapacity: 425
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Mahad',
    commissioningDate: '2017-05-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.64118,
      73.292388
    ],
    plantName: 'Akzo Nobel',
    state: 'Maharashtra',
    plantCapacity: 203
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bilaspur',
    commissioningDate: '2017-05-27T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.228742,
      82.147033
    ],
    plantName: 'PGCOIL',
    state: 'Chattisgarh',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Alwar',
    commissioningDate: '2017-06-16T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      27.511468,
      76.668552
    ],
    plantName: 'Walplast',
    state: 'Rajasthan',
    plantCapacity: 299.6
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Vadodara',
    commissioningDate: '2017-06-22T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.310455,
      73.18154
    ],
    plantName: 'Schneider MVI',
    state: 'Gujarat',
    plantCapacity: 359.1
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Vadodara',
    commissioningDate: '2017-06-22T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.310455,
      73.18154
    ],
    plantName: 'Schneider TBI',
    state: 'Gujarat',
    plantCapacity: 359.1
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2017-06-20T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.149609,
      78.212049
    ],
    plantName: 'Symbiosis Ujwal Test',
    state: 'Telangana',
    plantCapacity: 467.765
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Talegaon',
    commissioningDate: '2017-06-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.735261,
      73.674324
    ],
    plantName: 'MIT Talegaon 2',
    state: 'Maharashtra',
    plantCapacity: 308
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raipur',
    commissioningDate: '2017-06-25T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      21.258816,
      81.623126
    ],
    plantName: 'CTI',
    state: 'Chattisgarh',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raipur',
    commissioningDate: '2017-06-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      21.244212,
      81.64208
    ],
    plantName: 'PWDHO',
    state: 'Chhattisgarh',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Lunsapur',
    commissioningDate: '2017-07-25T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.938516,
      71.394453
    ],
    plantName: 'Sintex (100 KWp - AC)',
    state: 'Gujarat',
    plantCapacity: 128
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2017-07-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.424268,
      78.548449
    ],
    plantName: 'Fourth Partner House DG',
    state: 'Telangana',
    plantCapacity: 14.8
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Nagpur',
    commissioningDate: '2017-05-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.960227,
      79.014401
    ],
    plantName: 'TGCPOE - SECI',
    state: 'Maharashtra',
    plantCapacity: 120
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2017-05-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.575598,
      73.908371
    ],
    plantName: 'Symbiosis (Pune) - SECI',
    state: 'Maharashtra',
    plantCapacity: 207.9
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raichur',
    commissioningDate: '2017-05-22T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      16.203917,
      77.348915
    ],
    plantName: 'Navodaya Hospital - SECI',
    state: 'Karnataka',
    plantCapacity: 500
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raichur',
    commissioningDate: '2017-05-22T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      16.203917,
      77.348915
    ],
    plantName: 'Navodaya Residential - SECI',
    state: 'Karnataka',
    plantCapacity: 264
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bareilly',
    commissioningDate: '2017-05-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.292653,
      79.492833
    ],
    plantName: 'Invertis - SECI',
    state: 'Uttar Pradesh',
    plantCapacity: 800
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2017-05-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.149609,
      78.212049
    ],
    plantName: 'Symbiosis Hyd - SECI',
    state: 'Telengana',
    plantCapacity: 467.8
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raichur',
    commissioningDate: '2017-08-18T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      16.022027,
      76.391641
    ],
    plantName: 'Navodaya Hospital',
    state: 'Karnataka',
    plantCapacity: 500
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Raichur',
    commissioningDate: '2017-08-17T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      16.022027,
      76.391641
    ],
    plantName: 'Navodaya 2',
    state: 'Karnataka',
    plantCapacity: 264
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Jaipur',
    commissioningDate: '2017-08-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.885777,
      75.798257
    ],
    plantName: 'D- Mart (Pratap Nagar)',
    state: 'Rajasthan',
    plantCapacity: 39
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Ajmer',
    commissioningDate: '2017-08-05T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      25.370419,
      74.627287
    ],
    plantName: 'D-Mart (Ajmer)',
    state: 'Rajasthan',
    plantCapacity: 70
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Talegaon',
    commissioningDate: '2017-07-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.735059,
      73.674516
    ],
    plantName: 'Mit Talegaon - SECI',
    state: 'Maharashtra',
    plantCapacity: 500
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Alandi',
    commissioningDate: '2017-07-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.675013,
      73.892053
    ],
    plantName: 'MIT Alandi - SECI',
    state: 'Maharashtra',
    plantCapacity: 434.74
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bareilly',
    commissioningDate: '2017-05-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      28.292653,
      79.492833
    ],
    plantName: 'Invertis 300 - SECI',
    state: 'Uttar Pradesh',
    plantCapacity: 300
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bareilly',
    commissioningDate: '2017-05-11T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      28.292653,
      79.492833
    ],
    plantName: 'Invertis 500 - SECI',
    state: 'Uttar Pradesh',
    plantCapacity: 500
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Alandi',
    commissioningDate: '2017-08-08T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.67286,
      73.889156
    ],
    plantName: 'MIT Alandi 2',
    state: 'Maharashtra',
    plantCapacity: 225.6
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Kondhwa',
    commissioningDate: '2017-10-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.457834,
      73.892459
    ],
    plantName: 'D- Mart (Kondhwa)',
    state: 'Maharashtra',
    plantCapacity: 56.92
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Udaipur',
    commissioningDate: '2017-10-01T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      24.530416,
      73.774221
    ],
    plantName: 'S. S. College',
    state: 'Rajasthan',
    plantCapacity: 300
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Ghaziabad',
    commissioningDate: '2017-09-01T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.647553,
      77.377448
    ],
    plantName: 'Jaipuria Institute',
    state: 'Uttar Pradesh',
    plantCapacity: 170.88
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Daman',
    commissioningDate: '2017-10-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.413215,
      72.876627
    ],
    plantName: 'D-Mart (Daman)',
    state: 'Gujarat',
    plantCapacity: 32
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Chindwala',
    commissioningDate: '2017-09-19T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      21.554591,
      78.815025
    ],
    plantName: 'Walplast (Miraj - Chindwala)',
    state: 'Madhya Pradesh',
    plantCapacity: 233.6
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2017-10-11T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      18.518051,
      73.923203
    ],
    plantName: 'Eaton',
    state: 'Maharashtra',
    plantCapacity: 170.775
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Hyderabad',
    commissioningDate: '2017-10-24T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      17.538988,
      78.170823
    ],
    plantName: 'Niehoff',
    state: 'Telengana',
    plantCapacity: 151.2
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Rajkot',
    commissioningDate: '2017-10-29T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      22.310433,
      70.824257
    ],
    plantName: 'D-Mart (Rajkot)',
    state: 'Gujarat',
    plantCapacity: 71.82
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bhatinda',
    commissioningDate: '2017-11-05T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      30.251747,
      74.843663
    ],
    plantName: 'Baba Farid College Test',
    state: 'Panjab',
    plantCapacity: 152
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Jaipur',
    commissioningDate: '2017-11-06T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.978987,
      75.778969
    ],
    plantName: 'RTW PVt ltd',
    state: 'Rajasthan',
    plantCapacity: 50
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Zirakpur',
    commissioningDate: '2017-11-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      30.620403,
      76.821861
    ],
    plantName: 'D-Mart (Zirakpur)',
    state: 'Punjab',
    plantCapacity: 44.4
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bangalore',
    commissioningDate: '2017-11-14T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.893743,
      77.602912
    ],
    plantName: 'IIM Bangalore',
    state: 'Karnataka',
    plantCapacity: 291.2
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Chennai',
    commissioningDate: '2017-11-08T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      13.05958,
      80.141084
    ],
    plantName: 'Bigbasket Chennai',
    state: 'Tamil Nadu',
    plantCapacity: 99.2
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Chennai',
    commissioningDate: '2017-11-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.802355,
      79.909011
    ],
    plantName: 'Sintex BAPL',
    state: 'Tamil Nadu',
    plantCapacity: 609.92
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Urse',
    commissioningDate: '2017-11-25T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      18.71239,
      73.644957
    ],
    plantName: 'Emcure',
    state: 'Maharashtra',
    plantCapacity: 306.85
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Katni',
    commissioningDate: '2017-10-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_today',
    location: [
      23.891573,
      80.398964
    ],
    plantName: 'Walplast (Miraj - Katni)',
    state: 'Madhya Pradesh',
    plantCapacity: 147.5
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Bhilwara',
    commissioningDate: '2017-11-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      25.321765,
      74.587296
    ],
    plantName: 'D-Mart (Bhilwara)',
    state: 'Rajasthan',
    plantCapacity: 112
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Katni',
    commissioningDate: '2017-10-31T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      23.897808,
      80.402016
    ],
    plantName: 'Walplast (Viwa - Katni)',
    state: 'Madhya Pradesh',
    plantCapacity: 134.4
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Devanahalli',
    commissioningDate: '2017-11-27T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      13.268692,
      77.699955
    ],
    plantName: 'D-Mart (Devanahalli)',
    state: 'Karnataka',
    plantCapacity: 161.92
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Nellore',
    commissioningDate: '2017-12-03T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      14.429893,
      79.97126
    ],
    plantName: 'D-Mart (Nellore)',
    state: 'Andhra Pradesh',
    plantCapacity: 99.22
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Ongole',
    commissioningDate: '2017-10-15T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      15.486823,
      80.048303
    ],
    plantName: 'D-Mart (Ongole)',
    state: 'Andhra Pradesh',
    plantCapacity: 81.9
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Lucknow',
    commissioningDate: '2017-11-27T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.779324,
      80.835715
    ],
    plantName: 'T. S. Mishra - SECI',
    state: 'Uttar Pradesh',
    plantCapacity: 500.5
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Jaipur',
    commissioningDate: '2017-12-06T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.896471,
      75.796041
    ],
    plantName: 'RHDS Pump',
    state: 'Rajasthan',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pun',
    commissioningDate: '2017-12-06T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      392147,
      19237
    ],
    plantName: '4G Demo',
    state: 'MH',
    plantCapacity: 100
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Visakhapatnam',
    commissioningDate: '2017-11-20T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.81558,
      83.356047
    ],
    plantName: 'D-Mart (Madhurwada)',
    state: 'Andhra Pradesh',
    plantCapacity: 58.2
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Lucknow',
    commissioningDate: '2017-12-04T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.957821,
      80.999288
    ],
    plantName: 'Integral University',
    state: 'Uttar Pradesh',
    plantCapacity: 500
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Tirupati',
    commissioningDate: '2017-11-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      13.6220278,
      79.4209057
    ],
    plantName: 'D-Mart (Tirupati)',
    state: 'Aandra Pradesh',
    plantCapacity: 33.075
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Vapi',
    commissioningDate: '2017-12-09T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      20.483367,
      72.931935
    ],
    plantName: 'Raymond',
    state: 'Gujarat',
    plantCapacity: 640.64
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Shankarpalli',
    commissioningDate: '2017-12-18T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.453348,
      78.119545
    ],
    plantName: 'Ultratech',
    state: 'Telangana',
    plantCapacity: 1000
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Lucknow',
    commissioningDate: '2017-12-17T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      26.776615,
      80.842739
    ],
    plantName: 'T. S. Mishra',
    state: 'Uttar Pradesh',
    plantCapacity: 500.5
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Pune',
    commissioningDate: '2017-12-07T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      9458304,
      85967483
    ],
    plantName: 'test',
    state: 'Maharashtra',
    plantCapacity: 50
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Visakhapatnam',
    commissioningDate: '2017-11-30T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.7226383,
      83.3079683
    ],
    plantName: 'GVMC',
    state: 'Andhra Pradesh',
    plantCapacity: 1547
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Kanakapura',
    commissioningDate: '2017-12-22T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      12.875728,
      77.544532
    ],
    plantName: 'D-Mart (Kanakapura)',
    state: 'Karnataka',
    plantCapacity: 134.4
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'New Delhi',
    commissioningDate: '2017-12-26T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      28.613478,
      77.239617
    ],
    plantName: 'Mater Dei',
    state: 'New Delhi',
    plantCapacity: 101.1
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Visakhapatnam',
    commissioningDate: '2017-12-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.757555,
      83.175798
    ],
    plantName: 'GVMC TSR',
    state: 'Andhra Pradesh',
    plantCapacity: 787.84
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Visakhapatnam',
    commissioningDate: '2017-12-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.679322,
      83.148018
    ],
    plantName: 'GVMC',
    state: 'Andhra Pradesh',
    plantCapacity: 171.92
  },
  {
    addressLineOne: null,
    addressLineTwo: null,
    city: 'Visakhapatnam',
    commissioningDate: '2017-12-10T18:30:00.000Z',
    inverterEnergyCalculation: 'kwh_total',
    location: [
      17.757555,
      83.175798
    ],
    plantName: 'GVMC Main',
    state: 'Andhra Pradesh',
    plantCapacity: 258.18
  }
];
