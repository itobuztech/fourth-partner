import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

declare var Isotope, imagesLoaded;

@Component({
  selector: 'portfolio-client',
  templateUrl: './client.html'
})
export class PortfolioClient implements AfterViewInit {
  @ViewChild('isotopeWrapper') isotopeWrapper: ElementRef;
  selectOptions = {
    title: 'Filter Portfolio',
    cssClass: 'portfolio-select'
  }
  industries: {name: string, filterKey: string}[] = [];
  clients = [{
    name: 'assets/imgs/clients/industry/adler.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/eaton.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, { 
    name: 'assets/imgs/clients/industry/gilbarco.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/honeywell.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/nbc.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/pricol.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/rinac.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/salzer.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/schneider.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/sintex.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/walplast.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/industry/zonobel.jpg',
    industry: 'industrial',
    filterKey: 'industrial'
  }, {
    name: 'assets/imgs/clients/banks/anand-rathi.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/banks/axis-bank.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/banks/andhra-bank.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/banks/icici-bank.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/banks/indian-overseas-bank.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/banks/sbh.jpg',
    industry: 'banks & financial institutions',
    filterKey: 'banks'
  }, {
    name: 'assets/imgs/clients/educations/birla-institute.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/institute-of-chemical.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/bs-abdur-rahman.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/chennai.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/gokhale-education.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/griet.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/iilm.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/invertis.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/lnmiit.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/mit-pune.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/manipal.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/navodaya-education.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/oakridge.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/rajiv.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/sies.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/symbiosis.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/university-of-kerala.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/university-of-kota.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/educations/unknown.jpg',
    industry: 'education institutes',
    filterKey: 'education'
  }, {
    name: 'assets/imgs/clients/food/britannia.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/food/dimart.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/food/ferrero.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/food/nestle.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/food/pickwick.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/food/western.jpg',
    industry: 'food & beverages',
    filterKey: 'food'
  }, {
    name: 'assets/imgs/clients/gov/aiert.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/eil.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/bureau-of-india.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/indian-oil.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/mmtc.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/npc.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/ntpc.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/scope.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/gov/unknown.jpg',
    industry: 'psus & government',
    filterKey: 'psus'
  }, {
    name: 'assets/imgs/clients/pharma/glenmark.jpg',
    industry: 'pharma & healthcare',
    filterKey: 'pharma'
  }, {
    name: 'assets/imgs/clients/pharma/lupin.jpg',
    industry: 'pharma & healthcare',
    filterKey: 'pharma'
  }, {
    name: 'assets/imgs/clients/pharma/moolchand.jpg',
    industry: 'pharma & healthcare',
    filterKey: 'pharma'
  }, {
    name: 'assets/imgs/clients/retail/big-basket.jpg',
    industry: 'retail',
    filterKey: 'retail'
  }, {
    name: 'assets/imgs/clients/others/airtel.jpg',
    industry: 'others',
    filterKey: 'others'
  }, {
    name: 'assets/imgs/clients/others/bestech.jpg',
    industry: 'others',
    filterKey: 'others'
  }, {
    name: 'assets/imgs/clients/others/chloride-exide.jpg',
    industry: 'others',
    filterKey: 'others'
  }, {
    name: 'assets/imgs/clients/others/salzer.jpg',
    industry: 'others',
    filterKey: 'others'
  }, {
    name: 'assets/imgs/clients/others/ernst&young.jpg',
    industry: 'others',
    filterKey: 'others'
  }];
  private isotopeInstance;
  private selected: string = 'industrial';
  public get selectedIndustry() : string {
    return this.selected;
  }
  public set selectedIndustry(value) {
    this.selected = value;

    if (this.isotopeInstance.arrange) {
      this.isotopeInstance.arrange({
        filter: `.${value}` // Class selector
      });
    }
  }

  ngOnInit () {
    this.clients.forEach(client => {
      if (this.industries.findIndex(industry => industry.name === client.industry) === -1) {
        this.industries.push({
          name: client.industry,
          filterKey: client.filterKey
        });
      }
    });
  }

  ngAfterViewInit () {
    new imagesLoaded( this.isotopeWrapper.nativeElement, () => {
      this.isotopeInstance = new Isotope( this.isotopeWrapper.nativeElement, {
        itemSelector: '.clients--list',
        layoutMode: 'fitRows',
      });
      this.isotopeInstance.arrange({
        filter: `.${this.selectedIndustry}` // Class selector
      });
    });
  }
}
