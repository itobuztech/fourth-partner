import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PortfolioPage } from './portfolio';
import { PortfolioClient } from './clients/client';
import { PortfolioInstallation } from './installation/installation';

@NgModule({
  declarations: [
    PortfolioPage,
    PortfolioClient,
    PortfolioInstallation
  ],
  imports: [
    IonicPageModule.forChild(PortfolioPage),
  ],
})
export class PortfolioPageModule {}
