import { Component, ViewChild } from '@angular/core';

import { AlertController, Nav, Platform, LoadingController, MenuController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/combineLatest';

declare var Branch;

import { AuthserviceProvider } from '../providers/authservice/authservice';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild (Nav) nav: Nav;
  rootPage: string;
  pages: Array<{title: string, component?: any, icon: string, isLoggedInPage: boolean, action?: string, isDarkenPage: boolean, projectStatus?: boolean}>;
  isLoggedIn: boolean = false;
  activePage = false;
  hasStatusOpen: boolean = true;
  private pushObject: PushObject;
  private options: PushOptions = {
    android: {
      senderID: '778243581673',
      iconColor: '#11079484',
      icon: 'ic_notification'
    },
    ios: {
      alert: 'true',
      badge: true,
      sound: 'false'
    }
  };
  private activeUserStatus;
  public projectCount: number;
  private confirmExit;
  showSalesNav: boolean = false;

  salesNav = [{
    title: 'WELCOME',
    component: 'welcome',
    icon: 'fourpl-Welcome',
    isLoggedInPage: false,
    isDarkenPage: false
  },{
    title: 'CONTACT',
    component: 'contact',
    icon: 'fourpl-Contact',
    isLoggedInPage: false,
    isDarkenPage: true
  },{
    title: 'ABOUT SOLAR',
    component: 'aboutSolar',
    icon: 'fourpl-About-Solar',
    isLoggedInPage: false,
    isDarkenPage: true
  },{
    title: 'PORTFOLIO',
    component: 'portfolio',
    icon: 'fourpl-Portfolio',
    isLoggedInPage: false,
    isDarkenPage: true
  }];
  routingfromnotification = false;

  constructor(
    private alertController: AlertController,
    private menuController: MenuController,
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private push: Push,
    private loadingController: LoadingController,
    private authProvider: AuthserviceProvider
  ) { }

  ngOnInit() {
    // Wait for platform and call native code
    this.initApp();

    // Set up navigation items.
    this.pages = [{
      title: 'HOME',
      component: 'home',
      icon: 'fourpl-home',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: true
    }, {
      title: 'VAULT',
      component: 'vault',
      icon: 'fourpl-lock',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: true
    }, {
      title: 'VAULT',
      component: 'vault',
      icon: 'fourpl-lock',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: false
    }, {
      title: 'SERVICE REQUEST',
      component: 'service-request',
      icon: 'fourpl-cogwheel',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: false
    }, {
      title: 'PROFILE',
      component: 'Settings',
      icon: 'fourpl-social',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: true
    },{
      title: 'PROFILE',
      component: 'Settings',
      icon: 'fourpl-social',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: false
    },
    {
      title: 'DISCUSSION',
      component: 'discussion',
      icon: 'fourpl-people',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: true
    },{
      title: 'WELCOME',
      component: 'welcome',
      icon: 'fourpl-Welcome',
      isLoggedInPage: false,
      isDarkenPage: false
    },{
      title: 'CONTACT',
      component: 'contact',
      icon: 'fourpl-Contact',
      isLoggedInPage: false,
      isDarkenPage: true
    },{
      title: 'ABOUT SOLAR',
      component: 'aboutSolar',
      icon: 'fourpl-About-Solar',
      isLoggedInPage: false,
      isDarkenPage: true
    },{
      title: 'PLANT DETAILS',
      component: 'plantDetails',
      icon: 'fourpl-people',
      isLoggedInPage: true,
      isDarkenPage: false,
      projectStatus: false
    }, {
      title: 'PORTFOLIO',
      component: 'portfolio',
      icon: 'fourpl-Portfolio',
      isLoggedInPage: false,
      isDarkenPage: true
    },{
      title: 'SIGN IN',
      component: 'login',
      icon: 'fourpl-home',
      isLoggedInPage: false,
      isDarkenPage: true
    }];

    // Get user profile when token available.
    this.authProvider
      .token$
      .filter(token => !!token)
      .subscribe(profile => {
        this.authProvider.getProfile();
        this.menuController.swipeEnable(true);
        this.registerPush(true);
        this.isLoggedIn = true;
      });

    // Redirect user to login screen if token missing.
    this.authProvider
      .token$
      .filter(token => token === null)
      .subscribe(profile => {
        this.menuController.swipeEnable(false);
        this.isLoggedIn = false;
        this.hasStatusOpen = true;
        this.activeUserStatus = null;
        this.nav.setRoot('welcome');
      });

    this.routeByProjectStatus();
  }

  private initApp () {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString("#FFFFFF");
      this.splashScreen.hide();
      this.branchInit();

      if (this.platform.is('android')) {
        this.platform.registerBackButtonAction(() => {
          if (!this.nav.canGoBack()) {
            if (this.nav.getActive().id === 'home' || this.nav.getActive().id === 'service-request' || this.nav.getActive().id === 'welcome') {
              // Need to show alert.

              if (!this.confirmExit) {
                this.confirmExit = this.alertController.create({
                  title: 'Confirm',
                  message: 'Are you sure you want to exit the app?',
                  buttons: [
                    {
                      text: 'OK',
                      handler: () => this.platform.exitApp()
                    },
                    {
                      text: 'Cancel',
                      handler: () => this.confirmExit = undefined
                    }
                  ]
                });
                this.confirmExit.present();
              } else {
                this.confirmExit.dismiss();
                this.confirmExit = undefined;
              }
            } else {
              if (this.activeUserStatus === 'open') {
                this.nav.setRoot('home');
              } else if (this.activeUserStatus === 'close') {
                this.nav.setRoot('service-request');
              } else {
                this.nav.setRoot('welcome');
              }
            }
          } else {
            this.nav.pop();
          }
        }, 500);
      }
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    if (page.isDarkenPage) {
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString("#0A233E");
    } else {
      this.statusBar.backgroundColorByHexString("#FFFFFF");
      this.statusBar.styleDefault();
    }
  }

  checkActive(page) {
    this.activePage = !this.activePage;
  }

  loggedOut() {
    const loader = this.loadingController.create();
    loader.present();

    const token = localStorage.getItem('device_token');
    this.authProvider.pushDeRegister(token)
      .finally(() => {
        this.authProvider.logOut();
        loader.dismiss();
      })
      .subscribe(response => {
        localStorage.removeItem('device_token');
      }, err => {

      });
  }

  private routeByProjectStatus () {
    console.log('subscription ready')
    // Route user as per the project status
    this.authProvider.projects$
    .filter(values => values)
    .subscribe(profile => {
      // found token and profile
      console.log('routing by project status', profile)
      this.activeUserStatus = profile.status;
      if (profile.status === 'open') {
        if (!this.routingfromnotification) {
          this.nav.setRoot('home');
        }
        this.hasStatusOpen = false
      } else {
        // For now, service request needs to change to monitoring
        if (!this.routingfromnotification) {
          this.nav.setRoot('service-request');
        }
        this.hasStatusOpen = true
      }
      this.routingfromnotification = false;
    }, err => {})
  }

  registerPush (flag) {
    console.log('start registering push')
    if (flag) {
      this.pushObject = this.push.init(this.options);
      console.log('this.pushObject', this.pushObject);
      this.lookForNotifications();
    }
    this.pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    this.pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log('registration fired', registration);
        const device = {
          device_token: registration.registrationId,
          device_type: this.platform.is('android') ? 'android' : 'ios'
        };

        this.authProvider.pushInit(device).subscribe(response => {
          localStorage.setItem('device_token', registration.registrationId);
        }, err => {
          console.log('silently handling registration error. :)', err);
        });
        // }
      });
  }

  private switchFromNotification (additionalData) {
    const data = this.platform.is('android') ? additionalData : additionalData.extraPayLoad;
    this.authProvider.profile$.filter(values => values).subscribe(res => {
      const selectedProj = res.projects.find(x => x.id === Number(data.project_id));
      this.authProvider.selectProject(selectedProj);
      switch (data.type) {
        case 'discussions':
          this.nav.setRoot('discussion');
          break;

        case 'messages':
          this.nav.setRoot('ServiceRequestConversation', {
            id: data.typeId
          });
          break;

        case 'vault':
          this.nav.setRoot('vault', {
            vault: data.vaultId
          });
          break;

        case 'stages':
          this.nav.setRoot('home');
          break;

        default:
          this.nav.setRoot('home');
          break;
      }
    });
  }

  private lookForNotifications() {
    this.pushObject.on('notification')
      .subscribe((notification: any) => {
        console.log('notification fired', notification);
        if (notification.additionalData.foreground) {
          this.alertController.create({
            title: notification.title,
            message: notification.message,
            buttons: [{
              text: 'Cancel'
            }, {
              text: notification.additionalData.type == 'vault' ? 'View File':'View Message',
              handler: () => {
                this.switchFromNotification(notification.additionalData);
              }
            }]
          }).present();
        } else {
          console.log('notification background', notification);
          this.routingfromnotification = true;
          if (!notification.additionalData.coldstart) {
            this.switchFromNotification(notification.additionalData);
          } else {
              setTimeout(() => {
                console.log('switching from coldstart');
                this.switchFromNotification(notification.additionalData);
              }, 1000);
            }
        }
      });
  }

  private branchInit () {
    if (this.platform.is('cordova')) {
      // for development and debugging only
      Branch.setDebug(true);

      // Branch initialization
      Branch.initSession(data => {
        if (data.$doLogin && !this.isLoggedIn) {
          this.nav.setRoot('login');
          this.statusBar.backgroundColorByHexString("#0A233E");
        }
      });
    }
  }
}
