import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { ComponentsModule } from '../components/components.module';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Diagnostic } from '@ionic-native/diagnostic';
import { FileOpener } from '@ionic-native/file-opener';
import { Push } from '@ionic-native/push';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import 'chartjs-plugin-zoom';
import 'hammerjs';

import { MyApp } from './app.component';

import { httpFactory } from '../core/factory';

import { AuthserviceProvider } from '../providers/authservice/authservice';
import { VaultProvider } from '../providers/vaultsService';
import { DiscussionProvider } from '../providers/discussion/discussion';
import { DataProvider } from '../providers/dataService';
import { PlantsServiceProvider } from '../providers/plants-service/plants-service';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    ChartsModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileTransfer,
    File,
    Diagnostic,
    FileOpener,
    Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: Http, 
      useFactory: httpFactory, 
      deps: [XHRBackend, RequestOptions]
    },
    AuthserviceProvider,
    VaultProvider,
    DiscussionProvider,
    DataProvider,
    PlantsServiceProvider
  ]
})
export class AppModule {}
