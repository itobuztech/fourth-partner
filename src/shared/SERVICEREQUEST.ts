import { serviceRequest } from '../interface/serviceRequest';

export const SERVICEREQUEST: serviceRequest[] =[
  {
    orangeIcon: true,
    time: '10:45',
    category: '',
    head: 'foods-Restaurant And Food',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: false,
    time: '10:17',
    category: 'Cover Image',
    head: 'Online',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: true,
    time: '08:23',
    category: 'Studio',
    head: 'A Creative Agency',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: false,
    time: 'Fri',
    category: 'Total Fitness',
    head: 'All in one',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: true,
    time: 'Thu',
    category: 'GhostMag',
    head: 'Ghost Blog',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: false,
    time: 'Mon',
    category: 'GhostMag',
    head: 'Ghost Blog Theme',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
  {
    orangeIcon: true,
    time: '20 Oct',
    category: 'Redbee',
    head: 'Construction HTML',
    subhead: 'Lorem Ipsum is simply dummy text of',
  },
];
