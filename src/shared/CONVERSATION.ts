import { conversation } from '../interface/conversation';

export const CONVERSATION: conversation[] = [
  {
    icon: 'assets/imgs/orange-folder.png',
    head: 'Ifoods-Restaurant And Food WordPress Theme',
    author: '',
    time: 'May 10, 2017, 2:14 pm',
    content: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden.`,
    regards: 'Thanks and Regards',
  },
  {
    icon: '',
    head: 'John Doe',
    author: 'John Doe',
    time: 'May 10, 2017, 2:14 pm',
    content: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    regards: 'Thanks and Regards',
  },
];