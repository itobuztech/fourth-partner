import {Injectable} from "@angular/core";
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {environment} from "../core/environment";

@Injectable()
export class InterceptedHttp extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.get(url, this.getRequestOptionArgs(options, url));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.post(url, body, this.getRequestOptionArgs(options, url));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.put(url, body, this.getRequestOptionArgs(options, url));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.delete(url, this.getRequestOptionArgs(options, url));
    }

    private updateUrl(req: string): string {
        if (req.indexOf('api') === -1) {
            return environment.api_customer + req;
        } else {
            return environment.api_data + req;
        }
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs, url?: string) : RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('Content-Type', 'application/json');
        
        const token = localStorage.getItem('fourthpartner_token') ? localStorage.getItem('fourthpartner_token') : null;
        const plantToken = localStorage.getItem('fourthpartner_plantToken') ? localStorage.getItem('fourthpartner_plantToken') : null;

        if (url.indexOf(environment.api_customer) !== -1) {
            options.headers.append('Authorization', `Bearer ${token}`);
        } else {
            options.headers.append('Authorization', `Bearer ${plantToken}`);
        }


        return options;
    }
}
