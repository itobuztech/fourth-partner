import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthserviceProvider } from './authservice/authservice';

export interface serviceData {
  service_request_type_id: number,
  title: string
};
export interface DiscussionData {
  message: string,
  service_request_id: number
};
@Injectable()
export class DataProvider {
  constructor(public http: Http, private authService: AuthserviceProvider) { }

  private catchError (err) {
    if (err.status === 401) {
      this.authService.logOut();
    }

    err = err.json();
    return Observable.throw(err);
  }

  getStages(project_id) {
    return this.http.get(`project-stages/${project_id}`)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }

  // for creating service request
    createServiceRequest(payload: serviceData) {
      return this.http.post('service_requests', payload)
        .map(res => res.json())
        .map(res => res.data)
        .catch(this.catchError.bind(this))
    }

    //get service request by id
    getServiceRequestById(id) {
      return this.http.get(`service_requests/${id}`)
        .map(res => res.json())
        .map(res => res.data)
        .map(res => {
          const d = new Date(0);
          res.created_at_timestamp = d.setUTCSeconds(res.created_at_timestamp);

          return res;
        })
        .catch(this.catchError.bind(this)) 
    }

    getServiceType() {
      return this.http.get('service_request_types')
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this)) 
    }

    getServiceRequest() {
      return this.http.get('service_requests')
        .map(res => res.json())
        .map(res => res.data)
        .map(res => {
          if (res.data && res.data.length) {
            res.data = res.data.map(r => {
              const d = new Date(0);
              r.created_at_timestamp = d.setUTCSeconds(r.created_at_timestamp);
              return r;
            })
          }
          return res;
        })
        .catch(this.catchError.bind(this)) 
    }

    serviceConversation(serviceload: DiscussionData) {
      return this.http.post('service_messages', serviceload)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
    }

    getServiceMessage(service_request_id) {
      return this.http.get(`service_requests/${service_request_id}/messages`)
        .map(res => res.json())
        .map(res => res.data)
        .map(res => {
          return res.map(r => {
            const d = new Date(0);
            r.created_at_timestamp = d.setUTCSeconds(r.created_at_timestamp);
            return r;
          });
        })
        .catch(this.catchError.bind(this))
    }
}
