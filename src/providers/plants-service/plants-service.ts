import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/forkJoin';

import { AuthserviceProvider } from '../authservice/authservice';

@Injectable()
export class PlantsServiceProvider {
  constructor(
    public http: Http,
    private authserviceProvider: AuthserviceProvider
  ) { }

  getPlantDetail(powerPlantId) {
    let detail = `api/powerPlants/details/${powerPlantId}`

    return this.http.get(detail)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }

  /**
   * Get graph data
   * @param powerPlantId
   * @param graphType Available type: monthly | year | lifetime
   * @param date: Example 01/01/2018
   */
  getPlantChart (powerPlantId: string, graphType, date) {

    let url =`api/powerPlants/graph/${powerPlantId}?graphType=${graphType}&date=${date}`;
    return this.http.get(url)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }

  getPlantLineChart (powerPlantId: string, graphType, date) {
    let inverter = `api/powerPlants/graph/${powerPlantId}?graphType=${graphType}&date=${date}`;
    return this.http.get(inverter)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }

  private catchError (err) {
    if (err.status === 401) {
      this.authserviceProvider.logOut();
    }

    err = err.json();
    return Observable.throw(err);
  }
}
