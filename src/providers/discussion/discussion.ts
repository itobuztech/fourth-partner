import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/throw';

import { AuthserviceProvider } from '../authservice/authservice';

export interface DiscussionData {
  message: string,
  project_id: number
};

@Injectable()
export class DiscussionProvider {
  data = [];

  constructor(public http: Http, private authService: AuthserviceProvider) { }

  private catchError (err) {
    if (err.status === 401) {
      this.authService.logOut();
    }

    err = err.json();
    return Observable.throw(err);
  }

  /**
   * 
   * @param payload: Form Object 
   */
  sendDiscussion(payload: DiscussionData) {
    return this.http.post('discussions', payload)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }

  getDiscussion(id) {
    return this.http.get(`projects/${id}/discussions`)
      .map(res => res.json())
      .map(res => res.data)
      .catch(this.catchError.bind(this))
  }
}
