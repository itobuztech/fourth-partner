import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthserviceProvider } from './authservice/authservice';

@Injectable()
export class VaultProvider {
  constructor(public http: Http, private authService: AuthserviceProvider) { }

  private catchError (err) {
    if (err.status === 401) {
      this.authService.logOut();
    }

    err = err.json();
    return Observable.throw(err);
  }

  getVaults(project_id, vault_id?) {
    let url = `project-vaults/${project_id}`;
    if (vault_id) {
      url += `/${vault_id}`;
    }

    return this.http.get(url)
      .map(res => res.json())
      .map(res => res.data)
      .map(vaultItems => {
        return {
          children: vaultItems
        };
      })
      .catch(this.catchError.bind(this))
  }
}
