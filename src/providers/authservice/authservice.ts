import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthserviceProvider {
  private token = localStorage.getItem('fourthpartner_token') ? localStorage.getItem('fourthpartner_token') : null;
  private tokenSub: BehaviorSubject<string> = new BehaviorSubject(this.token);
  public token$ = this.tokenSub.asObservable();

  public profileSub: BehaviorSubject<any> = new BehaviorSubject(null);
  public profile$ = this.profileSub.asObservable();

  projectsSub: BehaviorSubject<any> = new BehaviorSubject(null);
  public projects$ = this.projectsSub.asObservable();

  projectId: any;

  constructor(
    public http: Http,
    private toastController: ToastController,
    ) {}

  login(formVal) {
    return this.http.post('login', formVal)
      .map(res => res.json())
      .map(res => res.data)
      .do(user => {
        localStorage.setItem('fourthpartner_token', user.token);
        this.tokenSub.next(user.token);
      })
      .catch(err => Observable.throw(err.json() || 'Unable to connect to server'));
  }

  logOut() {
    localStorage.clear();
    this.tokenSub.next(null);
    this.profileSub.next(null);
    this.projectsSub.next(null);
  }

  private catchError (err) {
    if (err.status === 401) {
      this.logOut();
    }

    err = err.json();
    return Observable.throw(err);
  }

  getAlertNotificationCount() {
    return this.http.get(`feedsCount/${this.projectId}`)
    .map(res => res.json())
    .map(res => res.data)
    .catch(this.catchError.bind(this))
  }

  getDiscussionNotificationCount() {
    return this.http.get(`get-notifications-count/${this.projectId}`)
    .map(res => res.json())
    .map(res => res.data)
    .catch(this.catchError.bind(this))
  }

  getProfile() {
    this.http.get('profile')
      .map(res => res.json())
      .map(res => res.data)
      .do(user => {
        localStorage.setItem('fourthpartner_plantToken', user.plantToken);
      })
      .catch(this.catchError.bind(this))
      .subscribe(profile => {
        console.log("profile", profile);
        if (profile.projects.length) {
          this.profileSub.next(profile);

          const prevSelectedProject = JSON.parse(localStorage.getItem('project'));
          const found = profile.projects.some(el => {
            return prevSelectedProject && (el.id === prevSelectedProject.id);
          }); //compare local storage data with available projects
          const selectedProject = found ? prevSelectedProject : profile.projects[0];
          console.log('Proj selected on app-open', selectedProject);
          this.selectProject(selectedProject)

          const payload = {project_id:selectedProject.id, type:"app-open"};
          this.addAnalytics(payload);
        } else {
          this.logOut();
          this.toastController.create({
            message: 'No projects available for the user',
            duration: 3000
          }).present();
        }
      }, err => {
        this.logOut();
        this.toastController.create({
          message: 'Invalid User',
          duration: 3000
        }).present();
      });
  }

  addAnalytics(payload) {
    return this.http.post('analytics', payload)
    .map(res => res.json())
    .map(res => res.data)
    .catch(this.catchError.bind(this))
    .subscribe(res => {
      console.log("Analytics", res);
    }, err => {
      console.log("err", err);
    });
  }

  passwordReset(value) {
    return this.http.post(`update-password`, value)
      .map(res => res.json())
      .catch(this.catchError.bind(this))
  }

  postEnquiry(enquiry) {
    return this.http.post('contact', enquiry)
      .map(res => res.json())
      .catch(this.catchError.bind(this))
  }

  getAlerts(projectId) {
    return this.http.get(`feeds/${projectId}`)
    .map(res => res.json())
    .map(res => res.data)
    .catch(this.catchError.bind(this))
  }

  pushInit (payload) {
    return this.http.post('register-device', payload)
      .map(res => res.json())
      .catch(this.catchError.bind(this))
  }

  pushDeRegister (device_token) {
    return this.http.post('deregister-device', { device_token })
      .map(res => res.json())
      .catch(this.catchError.bind(this))
  }

  selectProject (project) {
    this.projectId = project.id;
    if (this.projectsSub.value && project.id !== this.projectsSub.value.id) {
      this.projectsSub.next(project);
      localStorage.setItem('project', JSON.stringify(project));
    } else if (!this.projectsSub.value) {
      localStorage.setItem('project', JSON.stringify(project));
      this.projectsSub.next(project);
    }
  }
}
