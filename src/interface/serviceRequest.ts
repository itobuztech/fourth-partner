export interface serviceRequest {
 orangeIcon: boolean;
 time: string;
 category: string;
 head: string;
 subhead: string;
}