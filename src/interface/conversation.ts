export interface conversation {
  icon: string;
  head: string;
  author: string;
  time: string;
  content: string;
  regards: string;
}