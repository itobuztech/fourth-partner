export interface List {
  iconClass: string;
  num: number;
  head: string;
  subhead: string;
  exclamation: boolean;
  status: string;
}